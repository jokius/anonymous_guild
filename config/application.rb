# frozen_string_literal: true
require_relative 'boot'

require 'rails'
# Pick the frameworks you want:
require 'active_model/railtie'
require 'active_record/railtie'
require 'action_controller/railtie'
require 'action_mailer/railtie'
require 'action_view/railtie'
require 'action_cable/engine'
require 'sprockets/railtie'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Web
  class Application < Rails::Application
    config.time_zone = 'Moscow'
    I18n.enforce_available_locales = true
    config.action_cable.disable_request_forgery_protection = true
    config.active_job.queue_adapter = :sidekiq

    # Disable unnecessary generators
    config.generators do |g|
      g.assets = false
      g.helper = false
    end

    # Application locale
    I18n.default_locale = :ru
    I18n.locale = :ru

    # Enable console tweaks
    console do
      Bundler.require(:console)
      ActiveRecord::Base.logger = Logger.new(STDOUT)

      AwesomePrint.irb! if defined?(::AwesomePrint)
      Hirb.enable if defined?(::Hirb)
    end
  end
end
