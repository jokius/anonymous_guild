# frozen_string_literal: true
server '185.159.131.210',
       user: 'anonymous',
       roles: %w(web app db),
       primary: true

set :keep_releases, 10
set :deploy_to, '/var/www/anonymous/'

set :puma_bind, %w(tcp://0.0.0.0:3000)
set :puma_threads, [0, 16]
set :puma_workers, 1
