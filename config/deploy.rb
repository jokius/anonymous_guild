# frozen_string_literal: true
# config valid only for current version of Capistrano
lock '3.6.1'

set :application, 'anonymous'
set :scm, :git
set :repo_url, 'git@bitbucket.org:jokius/anonymous_guild.git'
set :keep_releases, 5
set :rbenv_ruby, '2.3.3'

ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp

# Default value for :linked_files is []
# Link Figaro config file
set :linked_files, fetch(:linked_files, []).push('config/application.yml')

# Default value for linked_dirs is []
set :linked_dirs, fetch(:linked_dirs, []).push('log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'vendor/bundle',
                                               'public/system', 'public/docs')

set :puma_preload_app, true
set :puma_init_active_record, true
