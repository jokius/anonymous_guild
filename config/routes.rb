# frozen_string_literal: true
Rails.application.routes.draw do
  devise_for :users, ActiveAdmin::Devise.config
  begin
    ActiveAdmin.routes(self)
  rescue
    ActiveAdmin::DatabaseHitDuringLoad
  end

  authenticate :user do
    mount Sidekiq::Web => '/sidekiq'
  end

  root 'dkp/dashboard#index'
  get '/auth/:provider/callback', to: 'dkp/users/omniauth_callbacks#create'
end
