# frozen_string_literal: true
ActiveAdmin.setup do |config|
  config.site_title = 'Anonymous DKP'
  config.default_namespace = :dkp
  config.authentication_method = :authenticate_user!
  config.current_user_method = :current_user
  config.logout_link_path = :destroy_user_session_path
  config.comments = false
  config.batch_actions = true
  config.current_filters = false
  config.localize_format = :long
  config.authorization_adapter = ActiveAdmin::PunditAdapter
  config.pundit_default_policy = 'ApplicationPolicy'
  config.load_paths = [
    File.expand_path('app/dkp', Rails.root)
  ]

  config.download_links = -> { current_user.available?(:show_download_links) ? [:csv] : false }
end
