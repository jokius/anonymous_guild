# frozen_string_literal: true
Rails.application.config.middleware.use OmniAuth::Builder do
  provider :vkontakte, ENV['VK_API_KEY'], ENV['VK_API_SECRET']
end
