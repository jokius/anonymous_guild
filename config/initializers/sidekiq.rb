# frozen_string_literal: true
Sidekiq.configure_server do |config|
  config.redis = { url: "redis://#{ENV['REDIS_HOST']}", namespace: "anonymous_guild_#{Rails.env}" }
end

Sidekiq.configure_client do |config|
  config.redis = { url: "redis://#{ENV['REDIS_HOST']}", namespace: "anonymous_guild_#{Rails.env}" }
end
