# frozen_string_literal: true
class AuctionChannel < ApplicationCable::Channel
  def subscribed
    stream_from :auctions
  end

  def unsubscribed
  end
end
