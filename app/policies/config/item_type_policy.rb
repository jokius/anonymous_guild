# frozen_string_literal: true
module Config
  class ItemTypePolicy < ApplicationPolicy
    def index?
      user.available?(:view_conf_item_type)
    end

    def show?
      false
    end

    def create?
      user.available?(:create_conf_item_type)
    end

    def update?
      user.available?(:update_conf_item_type)
    end

    def destroy?
      user.available?(:delete_conf_item_type)
    end
  end
end
