# frozen_string_literal: true
module Config
  class RaidPolicy < ApplicationPolicy
    def index?
      user.available?(:view_config_raids)
    end

    def show?
      false
    end

    def create?
      user.available?(:create_config_raid)
    end

    def update?
      user.available?(:update_config_raid)
    end

    def destroy?
      user.available?(:delete_config_raid) && record.raids.empty?
    end
  end
end
