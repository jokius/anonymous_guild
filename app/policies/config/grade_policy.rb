# frozen_string_literal: true
module Config
  class GradePolicy < ApplicationPolicy
    def index?
      user.available?(:view_config_grades)
    end

    def show?
      false
    end

    def create?
      user.available?(:create_config_grade)
    end

    def update?
      user.available?(:update_config_grade)
    end

    def destroy?
      user.available?(:delete_config_grade)
    end
  end
end
