# frozen_string_literal: true
module Config
  class ItemPolicy < ApplicationPolicy
    def index?
      user.available?(:view_conf_item)
    end

    def show?
      index?
    end

    def create?
      user.available?(:create_conf_item)
    end

    def update?
      user.available?(:update_conf_item)
    end

    def destroy?
      user.available?(:delete_conf_item)
    end
  end
end
