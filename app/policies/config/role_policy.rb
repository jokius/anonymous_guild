# frozen_string_literal: true
module Config
  class RolePolicy < ApplicationPolicy
    def index?
      user.available?(:view_config_roles)
    end

    def show?
      index?
    end

    def create?
      user.available?(:create_config_role)
    end

    def update?
      user.available?(:update_config_role)
    end

    def destroy?
      user.available?(:delete_config_role)
    end
  end
end
