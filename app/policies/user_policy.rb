# frozen_string_literal: true
class UserPolicy < ApplicationPolicy
  def index?
    user.active?
  end

  def show?
    record.id == user.id || user.active?
  end

  def create?
    false
  end

  def update?
    record.id == user.id || user.available?(:update_user_roles) || user.available?(:update_user_nickname)
  end

  def destroy?
    user.available?(:delete_user)
  end

  def activate?
    user.id != record.id && user.available?(:activate_user) && (record.pending? || record.blocked?)
  end

  def block?
    user.id != record.id && user.available?(:delete_user) && (record.pending? || record.active?)
  end

  def check_vk?
    record.id == user.id
  end

  def change_carma_or_points_form?
    user.id != record.id && carma_or_points_available? && record.active?
  end

  def change_carma_or_points?
    change_carma_or_points_form?
  end

  private

  def carma_or_points_available?
    user.available?(:add_carma) || user.available?(:add_points) || user.available?(:remove_carma) ||
      user.available?(:remove_points)
  end
end
