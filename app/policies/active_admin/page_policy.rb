# frozen_string_literal: true
module ActiveAdmin
  class PagePolicy < ApplicationPolicy
    def index?
      true
    end

    def show?
      case record.name
      when 'Dashboard'
        true
      when 'ConfigMenu'
        user.available?(:conf_memu)
      when 'Sidekiq'
        user.available?(:sidekiq)
      else
        user.active?
      end
    end
  end
end
