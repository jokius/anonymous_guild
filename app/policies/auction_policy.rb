# frozen_string_literal: true
class AuctionPolicy < ApplicationPolicy
  def index?
    user.active?
  end

  def show?
    false
  end

  def create?
    user.available? :create_auction
  end

  def update?
    false
  end

  def destroy?
    false
  end
end
