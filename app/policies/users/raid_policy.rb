# frozen_string_literal: true
module Users
  class RaidPolicy < ApplicationPolicy
    def index?
      user.active?
    end

    def update?
      record.raid.leader == user && record.user != user
    end

    def destroy?
      record.raid.leader != user
    end
  end
end
