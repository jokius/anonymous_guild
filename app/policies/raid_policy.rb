# frozen_string_literal: true
class RaidPolicy < ApplicationPolicy
  def index?
    user.active?
  end

  def show?
    false
  end

  def create?
    user.available? :create_raid
  end

  def update?
    record.open && user.available?(:update_raid)
  end

  def destroy?
    record.open && user.available?(:delete_raid)
  end

  def close?
    record.open && record.leader == user
  end
end
