# frozen_string_literal: true
class ApplicationPolicy
  attr_reader :user, :record

  def initialize(user, record)
    @user = user
    @record = record
  end

  delegate :index?, to: :policy
  delegate :show?, to: :policy
  delegate :create?, to: :policy
  delegate :update?, to: :policy
  delegate :destroy?, to: :policy

  def scope
    Pundit.policy_scope!(user, record.class)
  end

  def policy
    Pundit.policy!(user, record)
  end

  class Scope
    attr_reader :user, :scope

    def initialize(user, scope)
      @user = user
      @scope = scope
    end

    def resolve
      scope
    end
  end
end
