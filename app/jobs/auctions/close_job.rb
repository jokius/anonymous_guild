# frozen_string_literal: true
module Auctions
  class CloseJob < ApplicationJob
    queue_as :default

    def perform(id)
      Auctions::Close.run!(id: id)
    end
  end
end
