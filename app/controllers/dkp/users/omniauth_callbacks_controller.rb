# frozen_string_literal: true
module Dkp
  module Users
    class OmniauthCallbacksController < ApplicationController
      def create
        current_user.update(provider: auth_hash[:provider], uid: auth_hash[:uid])
        ::Users::CheckVk.run(user: current_user)
        redirect_to edit_dkp_user_path(current_user)
      end

      def failure
        redirect_to root_path
      end

      protected

      def auth_hash
        request.env['omniauth.auth']
      end
    end
  end
end
