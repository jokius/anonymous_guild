# frozen_string_literal: true
module Users
  class ChangeCarmaOrPoints < ActiveInteraction::Base
    object :user
    object :from_user, class: ::User
    integer :add_carma, :add_points, :remove_carma, :remove_points, default: 0

    def execute
      ActiveRecord::Base.transaction { change_carma }
      ActiveRecord::Base.transaction { change_points }
    end

    private

    def notifay(list, _email, vk)
      VkApi.new(user.uid).message list.map(&:description).join("\r\n") if vk && list.any?
    end

    def change_carma
      list = []
      add_carma = @add_carma.abs
      remove_carma = @remove_carma.abs
      if add_carma.positive?
        list << user.carma.create(value: add_carma,
                                  description: description('Добавлено', :carma_points, add_carma),
                                  from_user: from_user, name: :change_carma)
        user.increment!(:carma_count, add_carma)
      end

      if remove_carma.positive?
        list << user.carma.create(value: remove_carma,
                                  description: description('Снято', :carma_points, remove_carma),
                                  from_user: from_user, name: :change_carma)
        user.increment!(:carma_count, -remove_carma)
      end

      notifay(list, user.send_email_message?(:carma), user.send_vk_message?(:carma))
    end

    def change_points
      list = []
      add_points = @add_points.abs
      remove_points = @remove_points.abs
      if add_points.positive?
        list << user.points.create(value: add_points,
                                   description: description('Добавлено', :dkp_points, add_points),
                                   from_user: from_user, name: :change_points)
        user.increment!(:points_count, add_points)
      end

      if remove_points.positive?
        list << user.points.create(value: remove_points,
                                   description: description('Снято', :dkp_points, remove_points),
                                   from_user: from_user, name: :change_points)
        user.increment!(:points_count, -remove_points)
      end

      notifay(list, user.send_email_message?(:points), user.send_vk_message?(:points))
    end

    def description(status, type, points)
      "#{status} #{I18n.t(type, scope: :attributes, count: points)}. "\
      "#{I18n.t(:from_user, scope: :attributes, nickname: from_user.nickname)}."
    end
  end
end
