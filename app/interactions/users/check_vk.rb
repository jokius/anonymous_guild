# frozen_string_literal: true
module Users
  class CheckVk < ActiveInteraction::Base
    object :user

    def execute
      vk = VkApi.new user.uid
      vk.can_send? ? user.update(can_send: true) : vk.add_friend
    end
  end
end
