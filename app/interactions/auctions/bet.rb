# frozen_string_literal: true
module Auctions
  class Bet < ActiveInteraction::Base
    object :user
    integer :id, :points

    attr_reader :old_user

    def execute
      return result_error I18n.t(:not_found, scope: :auction) unless auction
      return result_error I18n.t(:not_enough_points, scope: :auction) if user.points_count < points
      return result_error I18n.t(:previous_more, scope: :auction) if auction.user_auction.try(:points).to_i >= points
      ActiveRecord::Base.transaction do
        user.increment! :points_count, -points
        @old_user = user_auction.user
        previous
        user_auction.points = points
        user_auction.user = user
        user_auction.save!
      end

      broadcast
      { status: :notice, message: I18n.t(:bid, scope: :auction) }
    end

    private

    def broadcast
      broadcast_hash = { auction_id: id, points: points }
      broadcast_hash = broadcast_hash.merge user_hash(:user, user)
      broadcast_hash = broadcast_hash.merge user_hash(:old_user, old_user) if old_user
      ActionCable.server.broadcast(:auctions, broadcast_hash)
    end

    def user_hash(parent_key, record)
      { parent_key => { id: record.id, nickname: record.nickname, points_count: record.points_count,
                        carma_count: record.carma_count } }
    end

    def auction
      @auction ||= Auction.find_by(id: id)
    end

    def result_error(message)
      { status: :error, message: message }
    end

    def user_auction
      @user_auction ||= auction.user_auction || auction.build_user_auction
    end

    def previous
      return unless old_user
      old_user.increment! :points_count, user_auction.points
      old_user.save!
    end
  end
end
