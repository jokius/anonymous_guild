# frozen_string_literal: true
module Auctions
  class Close < ActiveInteraction::Base
    integer :id

    def execute
      ActiveRecord::Base.transaction do
        auction.update open: false
        next unless user
        user.poins.create name: :auction, value: user_auction.points, description: description
      end

      send_notfications
      ActionCable.server.broadcast(:auctions, reload: true)
    end

    private

    def send_notfications
      return unless user.can_send || user.send_vk_message?(:auction_close)
      vk = VkApi.new(user.uid)
      vk.message("Потрачено #{I18n.t(:dkp_points, scope: :attributes, count: auction.user_auction.points)} за лот - "\
                 "#{auction.name}. Количество предметов #{auction.items_count}.")
    end

    def user_auction
      @user_auction ||= auction.user_auction
    end

    def user
      @user ||= user_auction.try(:user)
    end

    def auction
      @auction ||= Auction.find_by(id: id)
    end

    def description
      @description ||= "Потрачено #{I18n.t(:dkp_points, scope: :attributes, count: user_auction.points)} за лот - "\
                       "#{auction.name}. Количество предметов #{auction.items_count}."
    end
  end
end
