# frozen_string_literal: true
module Raids
  class Close < ActiveInteraction::Base
    object :raid

    attr_accessor :user, :role

    def execute
      ActiveRecord::Base.transaction do
        raid.users_raids.each do |ur|
          @user = ur.user
          @role = ur.role
          send_notfications
        end

        raid.update(open: false)
      end
    end

    private

    def send_notfications
      record_points = add_points
      record_carma = add_carma
      return unless user.can_send || user.send_vk_message?(:raid_close)
      vk = VkApi.new(user.uid)
      vk.message("Рейд завершен \n\r"\
                 "#{record_points.description} \n\r"\
                 "#{record_carma.description if record_carma}")
    end

    def add_points
      user_points = points
      user.increment!(:points_count, user_points)
      user.points.create name: :raid, value: user_points,
                         description: "Получено #{I18n.t(:dkp_points, scope: :attributes, count: user_points)} "\
                                     "за поход в рейд. #{I18n.t(:factor_dkp, scope: :attributes)} "\
                                     "за роль: #{role.factor_dkp}."
    end

    def add_carma
      user_carma = carma
      return if user_carma.zero?
      user.increment!(:carma_count, user_carma)
      user.carma.create name: :raid, value: user_carma,
                        description: "Получено #{I18n.t(:carma_points, scope: :attributes, count: user_carma)} "\
                                     "за поход в рейд. #{I18n.t(:factor_carma, scope: :attributes)} "\
                                     "за роль: #{role.factor_carma}."
    end

    def points
      (role.factor_dkp * raid.points).to_i
    end

    def carma
      (role.factor_carma * raid.carma)
    end
  end
end
