# frozen_string_literal: true
module Events
  class List < ActiveInteraction::Base
    object :user

    def execute
      list = []
      list.concat user.points.order(updated_at: :desc).limit(20)
      list.concat user.carma.order(updated_at: :desc).limit(20)
      list.concat Auction.opened.order(updated_at: :desc).limit(20)
      list.concat Raid.opened.order(updated_at: :desc).limit(20)
      list.sort { |a, b| b.updated_at <=> a.updated_at }.take(40)
    end
  end
end
