# frozen_string_literal: true
module Config
  class Raid < ApplicationRecord
    has_many :roles, class_name: 'Config::Raids::Role', dependent: :destroy
    has_many :raids, class_name: '::Raid', foreign_key: :config_raid_id
    accepts_nested_attributes_for :roles, allow_destroy: true
  end
end
