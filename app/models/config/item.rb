# frozen_string_literal: true
module Config
  class Item < ApplicationRecord
    belongs_to :grade, class_name: '::Config::Grade'
    belongs_to :item_type, class_name: '::Config::ItemType'

    validates :name, :description, presence: true
    validates :name, uniqueness: true, case_sensitive: false
  end
end
