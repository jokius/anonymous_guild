# frozen_string_literal: true
module Config
  module Roles
    def self.table_name_prefix
      'config_roles_'
    end
  end
end
