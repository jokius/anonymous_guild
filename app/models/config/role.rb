# frozen_string_literal: true
module Config
  class Role < ApplicationRecord
    has_many :user_roles, class_name: '::Users::Role', dependent: :destroy
    has_many :users, class_name: '::User', through: :user_roles

    has_many :roles_abilities, class_name: '::Config::Roles::Ability', dependent: :destroy
    has_many :abilities, class_name: '::Config::Ability', through: :roles_abilities

    scope :available, ->(wegth) { where('wegth < ?', wegth).order(wegth: :desc) }
  end
end
