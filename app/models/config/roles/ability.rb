# frozen_string_literal: true
module Config
  module Roles
    class Ability < ApplicationRecord
      belongs_to :role, class_name: '::Config::Role'
      belongs_to :ability, class_name: '::Config::Ability'

      after_save :plus_role_wegth
      after_destroy :minus_role_wegth

      def plus_role_wegth
        role.update(wegth: role.wegth + ability.wegth)
      end

      def minus_role_wegth
        role.update(wegth: role.wegth + ability.wegth)
      end
    end
  end
end
