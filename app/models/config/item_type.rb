# frozen_string_literal: true
module Config
  class ItemType < ApplicationRecord
    has_many :items, class_name: '::Config::Item'

    validates :name, presence: true
    validates :name, uniqueness: true, case_sensitive: false
  end
end
