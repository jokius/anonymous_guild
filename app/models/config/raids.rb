# frozen_string_literal: true
module Config
  module Raids
    def self.table_name_prefix
      'config_raids_'
    end
  end
end
