# frozen_string_literal: true
module Config
  class Ability < ApplicationRecord
    has_many :roles_abilities, class_name: '::Config::Roles::Ability', dependent: :destroy
    has_many :roles, class_name: '::Config::Roles', through: :roles_abilities
  end
end
