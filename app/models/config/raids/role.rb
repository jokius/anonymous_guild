# frozen_string_literal: true
module Config
  module Raids
    class Role < ApplicationRecord
      belongs_to :raid, class_name: 'Config::Raid'
    end
  end
end
