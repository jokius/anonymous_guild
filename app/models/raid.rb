# frozen_string_literal: true
class Raid < ApplicationRecord
  belongs_to :config_raid, class_name: '::Config::Raid'
  has_many :roles, class_name: '::Config::Raids::Role', through: :config_raid
  has_many :users_raids, class_name: '::Users::Raid', dependent: :destroy, autosave: true
  has_many :users, class_name: 'User', through: :users_raids
  delegate :carma, to: :config_raid

  scope :opened, -> { where(open: true) }
  scope :closed, -> { where(open: false) }

  validates :name, :pincode, :start, :points, presence: true
  validates :pincode, length: { maximum: 6 }
  validates :pincode, uniqueness: { scope: :open, message: 'ПИН-коды должны быть уникальные для открытых рейдов' }

  before_save :stop_set
  after_create :notify

  def stop_set
    return if hours_limit.zero?
    self.stop = start + hours_limit.hours
  end

  def available?
    users_limit.zero? || users.count < users_limit
  end

  def leader
    @leader ||= users_raids.find_by(role: leader_role).try(:user)
  end

  def leader_role
    @leader_role ||= roles.find_by(leader: true)
  end

  def leader=(leader_id)
    return if leader && leader.id == leader_id
    users_raids.find_by(user: leader).update role: roles.find_by(member: true) if leader
    new_leader = users_raids.find_by(user_id: leader_id)
    new_leader ? new_leader.update(role: leader_role) : users_raids.build(user_id: leader_id, role: leader_role)
  end

  def notify
    User.active.each do |user|
      RaidMailer.open(user, self).deliver_now if user.send_email_message? :raid_open
      next unless user.can_send || user.send_vk_message?(:raid_open)
      vk = VkApi.new(user.uid)
      vk.message('Создан рейд'\
                 "#{Raid.human_attribute_name(:config_raid)}: #{config_raid.name}.\r\n"\
                 "#{Raid.human_attribute_name(:points)}: #{points}.\r\n"\
                 "#{Raid.human_attribute_name(:leader)}: #{leader.nickname}.\r\n"\
                 "#{Raid.human_attribute_name(:start)}: #{I18n.l start, format: :long}.\r\n"\
                 "#{Raid.human_attribute_name(:stop)}: #{stop ? I18n.l(stop, format: :long) : I18n.t(:no_limit)}.\r\n")
    end
  end
end
