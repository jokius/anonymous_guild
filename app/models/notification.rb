# frozen_string_literal: true
class Notification < ApplicationRecord
  has_many :users_notifications, class_name: 'Users::Notification'
  has_many :users, class_name: 'User', through: :users_notifications
end
