# frozen_string_literal: true
class User < ApplicationRecord
  include AASM
  include ::Relationship::User
  include ::Devise::User

  after_create :notifications_set

  aasm column: :state do
    state :pending, initial: true
    state :active
    state :blocked

    event :activate do
      transitions from: [:pending, :blocked], to: :active
    end

    event :block do
      transitions from: [:pending, :active], to: :blocked
    end
  end

  def display_name
    name = "#{I18n.t(:dkp_points, scope: :attributes, count: points_count)} | "
    name += "#{I18n.t(:carma_points, scope: :attributes, count: carma_count)} | "
    name += nickname
    name += " #{I18n.t(state, scope: :attributes)}" if pending? || blocked?
    name
  end

  def add_role(role_name)
    role = Config::Role.find_or_create_by(name: role_name)
    return if roles.exists? role
    user_roles.create role: role
  end

  def available?(action)
    roles.joins(:abilities).where(config_abilities: { tech_name: action }).any?
  end

  def send_email_message?(notification)
    email_notifications.find_by(tech_name: notification).present?
  end

  def send_vk_message?(notification)
    vk_notifications.find_by(tech_name: notification).present?
  end

  def notifications_set
    Notification.all.each do |notification|
      users_notifications_email.create notification: notification, to: Users::Notification.tos[:email]
      users_notifications_vk.create notification: notification, to: Users::Notification.tos[:vk]
    end
  end
end
