# frozen_string_literal: true
class Auction < ApplicationRecord
  belongs_to :item, class_name: 'Config::Item'
  has_one :user_auction, class_name: 'Users::Auction'
  delegate :name, :grade, :item_type, :description, to: :item

  attr_reader :days_count
  attr_accessor :points

  scope :opened, -> { where(open: true) }
  scope :closed, -> { where(open: false) }

  after_create -> { Auctions::CloseJob.set(wait_until: close_time.to_i).perform_later(id) }
  after_create :notify

  def days_count=(value)
    self.close_time = Time.current + value.to_i.days + 3.hours
  end

  def notify
    User.active.each do |user|
      AuctionMailer.open(user, self).deliver_now if user.send_message? :auction_open
      next unless user.can_send || user.send_vk_message?(:auction_open)
      vk = VkApi.new(user.uid)
      vk.message('Создан лот'\
                 "#{Auction.human_attribute_name(:name)}: #{name}.\r\n"\
                 "#{Auction.human_attribute_name(:grade)}: #{grade.name}.\r\n"\
                 "#{Auction.human_attribute_name(:item_type)}: #{item_type.name}.\r\n"\
                 "#{Auction.human_attribute_name(:description)}: #{description}.")
    end
  end
end
