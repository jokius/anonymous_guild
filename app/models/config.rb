# frozen_string_literal: true
module Config
  def self.table_name_prefix
    'config_'
  end
end
