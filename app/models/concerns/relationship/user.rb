# frozen_string_literal: true
module Relationship
  module User
    extend ActiveSupport::Concern
    included do
      has_many :user_roles, class_name: 'Users::Role', dependent: :destroy
      has_many :roles, class_name: 'Config::Role', through: :user_roles
      accepts_nested_attributes_for :roles, allow_destroy: true

      has_many :points, class_name: 'Users::Point', dependent: :destroy
      has_many :carma, class_name: 'Users::Carma', dependent: :destroy
      has_many :user_raids, class_name: 'Users::Raid', dependent: :destroy
      has_many :raids, class_name: 'Raid', through: :user_raids
      has_many :user_auctions, class_name: 'Users::Auction'
      has_many :auctions, class_name: 'Auction', through: :user_auctions
      has_many :users_notifications_email, -> { where(to: Users::Notification.tos[:email]) }, class_name: 'Users::Notification'
      has_many :users_notifications_vk, -> { where(to: Users::Notification.tos[:vk]) }, class_name: 'Users::Notification'
      has_many :email_notifications, class_name: 'Notification', through: :users_notifications_email, source: :notification
      has_many :vk_notifications, class_name: 'Notification', through: :users_notifications_vk, source: :notification
    end
  end
end
