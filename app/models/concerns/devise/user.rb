# frozen_string_literal: true
module Devise
  module User
    extend ActiveSupport::Concern
    included do
      attr_writer :auth
      devise :database_authenticatable, :registerable,
             :recoverable, :rememberable, :trackable, :validatable

      validates :nickname, presence: true
      validates :nickname, uniqueness: true, case_sensitive: false
      validates_format_of :nickname, with: /\A\p{Cyrillic}*\z|\A[a-zA-Z]*\z/, multiline: true

      def auth
        @auth || nickname || email
      end

      def self.find_for_database_authentication(warden_conditions)
        conditions = warden_conditions.dup.to_h
        auth = conditions.delete(:auth)
        if auth
          where(conditions).where('lower(nickname) = :value OR lower(email) = :value', value: auth.downcase).first
        elsif conditions.key?(:nickname) || conditions.key?(:email)
          where(conditions).first
        end
      end
    end
  end
end
