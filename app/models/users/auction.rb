# frozen_string_literal: true
class Users::Auction < ApplicationRecord
  belongs_to :acution, class_name: '::Acution'
  belongs_to :user, class_name: '::User'
end
