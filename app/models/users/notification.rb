# frozen_string_literal: true
module Users
  class Notification < ApplicationRecord
    belongs_to :user, class_name: '::User'
    belongs_to :notification, class_name: '::Notification'

    enum to: %i(email vk)
  end
end
