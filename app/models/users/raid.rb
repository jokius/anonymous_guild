# frozen_string_literal: true
module Users
  class Raid < ApplicationRecord
    belongs_to :user, class_name: '::User'
    belongs_to :raid, class_name: '::Raid'
    belongs_to :role, class_name: '::Config::Raids::Role'
  end
end
