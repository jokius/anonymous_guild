# frozen_string_literal: true
module Users
  class Role < ApplicationRecord
    belongs_to :user, class_name: '::User'
    belongs_to :role, class_name: '::Config::Role'
  end
end
