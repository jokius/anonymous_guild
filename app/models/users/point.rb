# frozen_string_literal: true
module Users
  class Point < ApplicationRecord
    belongs_to :user
    belongs_to :from_user, class_name: '::User', required: false
  end
end
