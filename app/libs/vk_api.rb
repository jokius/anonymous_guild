# frozen_string_literal: true
class VkApi
  attr_accessor :uid

  def initialize(uid)
    @uid = uid
  end

  def can_send?
    vk.users.get(user_ids: uid, fields: :can_write_private_message)[0].try(:can_write_private_message).to_i == 1
  end

  def message(message)
    return if uid.blank?
    vk.messages.send(user_id: uid, message: message)
  rescue VkontakteApi::Error => e
    Rails.logger.error e
    User.find_by(uid: uid).update(can_send: false) if['error'] && e['error']['error_code'] == 7
  end

  def add_friend
    vk.friends.add(user_id: uid, text: 'Для получения уведомлений добавьте в друзья')
  end

  private

  def vk
    @vk ||= VkontakteApi::Client.new(ENV['VK_TOKEN'])
  end
end
