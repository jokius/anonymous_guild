App.cable.subscriptions.create { channel: "AuctionChannel" },
  received: (data) ->
    return unless @isAcution()
    return location.reload() if data.reload
    $("#link_user-js-#{data.auction_id}").
      html("<a href=\"/dkp/users/#{data.user.id}\">#{data.user.nickname}</a>")
    $("#current_points-js-#{data.auction_id}").html(data.points)

    if $("#bet-js-#{data.auction_id}").data('userid') == data.user.id
      $("#string-bet-js-#{data.auction_id}").show()
      $("#form-bet-js-#{data.auction_id}").hide()
      @currentUserLink(data.user)
    else
      $("#form-bet-js-#{data.auction_id}").show()
      $("#bet-input-js-#{data.auction_id}").val('')
      $("#string-bet-js-#{data.auction_id}").hide()
      return unless @isOldUser(data.old_user.id)
      @currentUserLink(data.old_user)

  isAcution: ->
    window.location.pathname == '/dkp/auctions' &&
    (window.location.search == '' ||
      window.location.search.search( /scope=open/i ))

  isOldUser: ->
    parseInt($('#current_user').html().split(/dkp\/users\/(\d+)/)[1]) ==
      oldUserId

  currentUserLink: (user) ->
    $('#current_user').
      html("<a href=\"/dkp/users/#{user.id}\">" +
           "#{user.points_count} "+
           "очков DKP | #{user.carma_count} очков кармы | "+
           "#{user.nickname}</a>")
