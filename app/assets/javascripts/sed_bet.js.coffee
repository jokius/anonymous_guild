jQuery(document).ready ($) ->
  $('.bet-js').each (_index, bet)->
    id = $(bet).data('id')
    currentBet = $("#bet-js-#{id}")
    $(currentBet.find('.bet-submit-js')).on 'click', ->
      currentVal = $(currentBet.find('.bet-input-js')[0]).val()
      return if currentVal == ''
      $.ajax(
        type: 'PUT'
        url: "/dkp/auctions/#{id}/bid"
        data:
          points: currentVal
      ).done (data)->
        $('.flashes').html('').
        append "<div class=\"flash flash_#{data.status}\">#{data.message}</div>"
