$(document).ready ->
  $('.role_change-js').change ->
    optionSelected = $(this).find("option:selected")
    $.ajax(
      type: 'PUT'
      url: "/dkp/raids/#{$(this).data('raidid')}"+
           "/users_raids/#{$(this).data('id')}"
      data:
        users_raid:
          role_id: optionSelected.val()
    )
