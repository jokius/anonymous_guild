# frozen_string_literal: true
ActiveAdmin.register_page 'ConfigMenu' do
  menu label: I18n.t(:config, scope: %i(active_admin menu)), priority: 9999, url: '#'
end
