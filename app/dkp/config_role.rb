# frozen_string_literal: true
ActiveAdmin.register Config::Role do
  menu parent: 'ConfigMenu'

  config.sort_order = 'id_asc'
  all_abilities = Config::Ability.all.order(wegth: :desc)

  controller do
    def permitted_params
      params.permit!
    end
  end

  index do
    id_column
    column :name
    column :users do |role|
      link_to role.users.count, dkp_users_path(scope: role.name.parameterize('_'))
    end

    actions
  end

  show do
    attributes_table do
      row :id
      row :name
      row :users do |role|
        link_to role.users.count, dkp_users_path(scope: role.name.parameterize('_'))
      end

      all_abilities.each do |ability|
        row(ability.name) { |role| status_tag(role.abilities.exists?(ability) ? :yes : :no) }
      end
    end
  end

  filter :name

  form do |f|
    f.inputs do
      f.input :name
      f.input :abilities, as: :check_boxes, collection: all_abilities
    end

    f.actions
  end
end
