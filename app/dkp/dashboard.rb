# frozen_string_literal: true
ActiveAdmin.register_page 'Dashboard' do
  menu priority: 1, label: proc { I18n.t(:dashboard, scope: :active_admin) }

  content title: proc { I18n.t(:dashboard, scope: :active_admin) } do
    div class: 'event_list' do
      events = Events::List.run!(user: current_user)
      events.each do |event|
        div class: 'event' do
          if event.is_a?(Users::Point) || event.is_a?(Users::Carma)
            event.description
          elsif event.is_a?(Auction)
            'Создан лот <br>'\
            "#{Auction.human_attribute_name(:name)}: #{event.name}. <br>"\
            "#{Auction.human_attribute_name(:grade)}: #{event.grade.name}. <br>"\
            "#{Auction.human_attribute_name(:item_type)}: #{event.item_type.name}. <br>"\
            "#{Auction.human_attribute_name(:description)}: #{event.description}.".html_safe
          elsif event.is_a?(Raid)
            'Создан рейд <br>'\
            "#{Raid.human_attribute_name(:name)}: #{event.name}. <br>"\
            "#{Raid.human_attribute_name(:config_raid)}: #{event.config_raid.name}. <br>"\
            "#{Raid.human_attribute_name(:points)}: #{event.points}. <br>"\
            "#{Raid.human_attribute_name(:leader)}: #{event.leader.nickname}. <br>"\
            "#{Raid.human_attribute_name(:start)}: #{I18n.l(event.start, format: :long)}. <br>"\
            "#{Raid.human_attribute_name(:stop)}: "\
            "#{event.stop ? I18n.l(event.stop, format: :long) : I18n.t(:no_limit)}.".html_safe
          end
        end

        div(class: 'event-date') { I18n.l(event.created_at, format: :long) }
      end

      div(class: 'event-empty') { 'Сдеcь будут ваши события и события гильдии' } if events.empty?
    end
  end # content
end
