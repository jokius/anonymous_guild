# frozen_string_literal: true
ActiveAdmin.register Auction do
  menu priority: 4
  actions :index, :create, :new

  scope :opened, default: true
  scope :closed

  controller do
    def permitted_params
      params.permit!
    end
  end

  index do
    column :name
    column :grade
    column :item_type
    column :items_count
    column :description
    column(:close_time) do |auction|
      status_tag '', :ok, class: 'colse_timer-js', 'data-time' => auction.close_time.utc.strftime('%Y/%m/%d %H:%M:%S')
    end
    column(:user) do |auction|
      user = auction.user_auction.try(:user)
      div(id: "link_user-js-#{auction.id}") do
        user ? link_to(user.nickname, dkp_user_path(user)) : I18n.t(:user_not_set)
      end
    end

    column(:current_points) do |auction|
      div(id: "current_points-js-#{auction.id}") do
        auction.user_auction ? auction.user_auction.points : auction.start_price
      end
    end
    column(:bet) do |auction|
      div class: 'bet-js', id: "bet-js-#{auction.id}", 'data-id' => auction.id, 'data-userid' => current_user.id do
        style_hash = if auction.user_auction && auction.user_auction.user == current_user
                       { string: :block, form: :none }
                     else
                       { string: :none, form: :block }
                     end

        div(id: "string-bet-js-#{auction.id}", style: "display: #{style_hash[:string]}") do
          status_tag(I18n.t(:your_bid_auction, scope: :auction), :green)
        end

        div(id: "form-bet-js-#{auction.id}", style: "display: #{style_hash[:form]}") do
          semantic_form_for [:dkp, auction], url: bid_dkp_auction_path(auction) do |f|
            div f.input(:points, as: :number, label: false,
                                 input_html: { class: 'bet-input-js', id: "bet-input-js-#{auction.id}" })
            div class: 'fieldset' do
              link_to I18n.t(:bet, scope: :attributes), 'javascript:void(0)',
                      class: ' button_action button bet-submit-js'
            end
          end
        end
      end
    end
  end

  filter :item

  form do |f|
    f.inputs do
      f.input :item, include_blank: false
      f.input :items_count
      f.input :start_price
      f.input :days_count, as: :select, collection: (1..7).map { |number| [I18n.t(:days, count: number), number] },
                           input_html: { class: 'default-select' }, include_blank: false
    end

    f.actions
  end

  member_action :bid, method: :put do
    render json: Auctions::Bet.run!(params.merge(user: current_user))
  end
end
