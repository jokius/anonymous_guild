# frozen_string_literal: true
ActiveAdmin.register User do
  menu priority: 2
  config.sort_order = 'nickname_asc'
  all_roles = Config::Role.all
  controller do
    def update_resource(object, attributes)
      update_method = attributes.first[:password].present? ? :update_attributes : :update_without_password
      object.send(update_method, *attributes)
    end

    def permitted_params
      params.permit!
    end
  end

  scope :active, default: true
  scope :pending
  scope :blocked
  scope(I18n.t(:me)) { |scope| scope.where(id: current_user.id) }
  all_roles.each do |role|
    scope(role.name) { |scope| scope.joins(:roles).where(config_roles: { name: role.name }).distinct }
  end

  index do
    id_column
    column :nickname
    column :state do |user|
      color =
        if user.pending?
          :warn
        elsif user.active?
          :green
        elsif user.blocked?
          :red
        end

      status_tag(I18n.t(user.state, scope: :attributes), color)
    end

    column :points_count
    column :carma_count
    actions do |user|
      if Pundit.policy!(current_user, user).activate?
        item I18n.t(:activate, scope: :attributes), activate_dkp_user_path(user), class: 'member_link'
      end

      if Pundit.policy!(current_user, user).block?
        item I18n.t(:block, scope: :attributes), block_dkp_user_path(user), class: 'member_link'
      end

      if Pundit.policy!(current_user, user).change_carma_or_points_form?
        item I18n.t(:change_carma_or_points, scope: :attributes), change_carma_or_points_form_dkp_user_path(user),
             class: 'member_link'
      end
    end
  end

  show do
    attributes_table do
      row :id
      row :nickname
      row :state do |user|
        color =
          if user.pending?
            :warn
          elsif user.active?
            :green
          elsif user.blocked?
            :red
          end

        status_tag(I18n.t(user.state, scope: :attributes), color)
      end

      row :email if current_user == resource || current_user.available?(:view_email_user)
      row :points_count
      row :carma_count
      list_row(:roles) { |user| user.roles.pluck(:name) }
    end
  end

  filter :nickname

  form partial: 'form'

  member_action :activate do
    Pundit.authorize current_user, resource, :activate?
    resource.activate!
    redirect_to :back, notice: t(:user_activated, scope: %i(active_admin notices))
  end

  member_action :block do
    Pundit.authorize current_user, resource, :block?
    resource.block!
    redirect_to :back, notice: t(:user_blocked, scope: %i(active_admin notices))
  end

  member_action :check_vk do
    Users::CheckVk.run(user: resource)
    redirect_to edit_dkp_user_path(resource)
  end

  member_action :change_carma_or_points_form do
    @page_title = "#{resource.nickname}: #{I18n.t(:change_carma_or_points, scope: :attributes)}"
    @user = resource
  end

  member_action :change_carma_or_points, method: :post do
    hash = params[:change_carma_or_points].select { |_, value| !value.to_i.zero? }
    Users::ChangeCarmaOrPoints.run!(hash.merge(from_user: current_user, user: resource))
    redirect_to dkp_users_path
  end
end
