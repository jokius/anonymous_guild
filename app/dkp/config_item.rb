# frozen_string_literal: true
ActiveAdmin.register Config::Item do
  menu parent: 'ConfigMenu'
  actions :all
  permit_params :name, :description, :item_type_id, :grade_id

  index do
    column :name
    column :item_type
    column :grade
    actions
  end

  show do
    attributes_table do
      row :name
      row :item_type
      row :grade
      row :description
    end
  end

  filter :name

  form do |f|
    f.inputs do
      f.input :name
      f.input :item_type, include_blank: false
      f.input :grade, include_blank: false
      f.input :description
    end

    f.actions
  end
end
