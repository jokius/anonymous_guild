# frozen_string_literal: true
ActiveAdmin.register Raid do
  menu priority: 3
  actions :all, except: :show

  sidebar I18n.t(:activate_pincode, scope: %i(active_admin sidebar)), partial: 'activate_pincode_sidebar',
                                                                      only: :index, priority: 0, class: 'pincode_form'

  controller do
    def permitted_params
      params.permit!
    end
  end

  scope :opened, default: true
  scope :closed

  index do
    column :config_raid
    column :name
    column :points
    column :start
    column(:stop) { |raid| raid.stop ? raid.stop : I18n.t(:no_limit) }
    column(:leader) { |raid| link_to raid.leader.nickname, dkp_user_path(raid.leader) }
    column(:users) do |raid|
      link_to I18n.t(:users_in_raid, scope: :attributes, count: raid.users.count),
              dkp_raid_users_raids_path(raid)
    end

    actions do |raid|
      if Pundit.policy!(current_user, raid).close?
        item I18n.t(:close, scope: :attributes), close_dkp_raid_path(raid)
      end
    end
  end

  filter :name
  filter :points

  form do |f|
    f.inputs do
      f.input :config_raid, as: :select, include_blank: false
      f.input :pincode, input_html: { id: 'pincode' }
      f.input :name
      f.input :points, hint: 'на одного участника'
      f.input :leader, as: :select, include_blank: false, collection: User.active.pluck(:nickname, :id),
                       selected: resource.leader.try(:id) || current_user.id
      f.input :start, as: :date_time_picker
      f.input :hours_limit, hint: '0 - не ограничено'
    end

    f.actions
  end

  collection_action :activate, method: :post do
    raid = Raid.find_by(open: true, pincode: params[:raide][:pincode]) if params[:raide][:pincode].present?
    if raid
      if raid.available?
        current_user.user_raids.find_or_create_by raid: raid
        flash[:notice] = 'Вы добавлены в рейд'
      else
        flash[:error] = 'Нет сбодных мест'
      end
    else
      flash[:error] = 'ПИН-код не найден'
    end

    redirect_to collection_path
  end

  member_action :close do
    Pundit.authorize current_user, resource, :close?
    Raids::Close.run!(raid: resource)
    redirect_to :back, notice: t(:raid_close, scope: %i(active_admin notices))
  end
end
