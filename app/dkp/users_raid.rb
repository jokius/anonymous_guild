# frozen_string_literal: true
ActiveAdmin.register Users::Raid do
  belongs_to :raid, optional: true
  menu false

  actions :index, :update, :destroy
  permit_params :role_id

  index do
    column(:user) { |ur| link_to ur.user.nickname, dkp_user_path(ur.user) }
    column(:role) do |ur|
      leader = ur.raid.leader
      next ur.role.name if leader != current_user || ur.user == current_user
      semantic_form_for [:dkp, ur] do |f|
        f.input :role, include_blank: false, label: false, collection: ur.raid.roles.where(leader: false),
                       input_html: { class: 'role_change-js', data: { id: ur.id, raidid: ur.raid.id } }
      end
    end
  end

  config.filters = false

  action_item :leave_button, only: :index do
    ur = raid.users_raids.find_by(user: current_user)
    if ur && Pundit.policy(current_user, ur).destroy?
      link_to I18n.t(:leave_raid), dkp_raid_users_raid_path(raid, ur), method: :delete
    end
  end
end
