# frozen_string_literal: true
ActiveAdmin.register Config::Grade do
  config.sort_order = 'id_asc'

  menu parent: 'ConfigMenu'
  actions :all, except: :show
  permit_params :name

  index do
    column :name
    actions
  end

  filter :name

  form do |f|
    f.inputs do
      f.input :name
    end

    f.actions
  end
end
