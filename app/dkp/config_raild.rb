# frozen_string_literal: true
ActiveAdmin.register Config::Raid do
  menu parent: 'ConfigMenu'

  actions :all, except: :show

  controller do
    def permitted_params
      params.permit!
    end
  end

  index do
    id_column
    column :name
    column :carma
    column(:roles) { |raid| raid.roles.pluck(:name).join(', ') }
    actions dropdown: true
  end

  filter :name
  filter :carma

  form do |f|
    f.inputs do
      f.input :name
      f.input :carma
      f.has_many :roles, heading: Config::Raids::Role.model_name.human(count: 2) do |role|
        role.input :_destroy, as: :boolean, label: 'Удалить?' if role.object.present?
        role.input :name
        role.input :factor_dkp
        role.input :factor_carma
        role.input :leader
        role.input :member
      end
    end

    f.actions
  end
end
