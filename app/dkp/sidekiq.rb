# frozen_string_literal: true
ActiveAdmin.register_page 'Sidekiq' do
  menu parent: 'ConfigMenu'

  content do
    iframe src: '/sidekiq', width: '100%', height: '1080px'
  end
end
