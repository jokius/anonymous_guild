# frozen_string_literal: true
class ApplicationMailer < ActionMailer::Base
  default from: "Гильдия Anonymous <#{ENV['EMAIL'] || 'from@example.com'}>"
  layout 'mailer'
end
