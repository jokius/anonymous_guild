# frozen_string_literal: true
class RaidMailer < ApplicationMailer
  def open(user, raid)
    @raid = raid
    mail to: user.email, subject: "Cоздан рейд - #{@raid.name}"
  end

  def close(user, points, carma)
    @points = points
    @carma = carma
    mail to: user.email, subject: 'Рейд закрыт'
  end
end
