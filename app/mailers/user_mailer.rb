# frozen_string_literal: true
class UserMailer < ApplicationMailer
  def change_carma_or_points(user, description)
    mail to: user.email, subject: 'Изменение кармы или очков DKP', body: description
  end
end
