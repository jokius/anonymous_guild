# frozen_string_literal: true
class AuctionMailer < ApplicationMailer
  def open(user, auction)
    @auction = auction
    mail to: user.email, subject: "Cоздан лот - #{@auction.name}"
  end

  def close(user, auction)
    @auction = auction
    mail to: user.email, subject: "Вы выйграли лот - #{@auction.name}"
  end
end
