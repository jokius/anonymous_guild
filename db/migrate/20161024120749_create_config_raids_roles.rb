# frozen_string_literal: true
class CreateConfigRaidsRoles < ActiveRecord::Migration[5.0]
  def change
    create_table :config_raids_roles do |t|
      t.belongs_to :raid, index: true
      t.string :name
      t.float :factor_dkp, default: 1
      t.float :factor_carma, default: 1
      t.boolean :leader, default: false
      t.boolean :member, default: false

      t.timestamps
    end
  end
end
