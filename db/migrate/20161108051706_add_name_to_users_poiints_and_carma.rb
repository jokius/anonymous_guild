# frozen_string_literal: true
class AddNameToUsersPoiintsAndCarma < ActiveRecord::Migration[5.0]
  def change
    add_column :users_points, :name, :string
    add_column :users_carmas, :name, :string
  end
end
