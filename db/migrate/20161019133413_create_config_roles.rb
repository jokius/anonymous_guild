# frozen_string_literal: true
class CreateConfigRoles < ActiveRecord::Migration
  def change
    create_table :config_roles do |t|
      t.string :name
      t.integer :wegth, default: 0

      t.timestamps
    end
  end
end
