# frozen_string_literal: true
class CreateConfigRolesAbilities < ActiveRecord::Migration[5.0]
  def change
    create_table :config_roles_abilities do |t|
      t.belongs_to :role, index: true
      t.belongs_to :ability, index: true

      t.timestamps
    end

    add_index :config_roles_abilities, %i(role_id ability_id)
  end
end
