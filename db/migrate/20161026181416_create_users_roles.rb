# frozen_string_literal: true
class CreateUsersRoles < ActiveRecord::Migration[5.0]
  def change
    create_table :users_roles do |t|
      t.belongs_to :user, index: true
      t.belongs_to :role, index: true

      t.timestamps
    end

    add_index :users_roles, %i(user_id role_id)
  end
end
