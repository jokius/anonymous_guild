class AddToToUsersNotification < ActiveRecord::Migration[5.0]
  def change
    add_column :users_notifications, :to, :integer
  end
end
