# frozen_string_literal: true
class CreateConfigRaids < ActiveRecord::Migration[5.0]
  def change
    create_table :config_raids do |t|
      t.string :name
      t.integer :carma

      t.timestamps
    end
  end
end
