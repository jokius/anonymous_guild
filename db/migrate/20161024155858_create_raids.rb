# frozen_string_literal: true
class CreateRaids < ActiveRecord::Migration[5.0]
  def change
    create_table :raids do |t|
      t.belongs_to :config_raid, index: true
      t.string :name
      t.integer :pincode
      t.datetime :start
      t.datetime :stop
      t.integer :points
      t.integer :hours_limit, default: 0
      t.integer :users_limit, default: 0
      t.boolean :open, default: true

      t.timestamps
    end

    add_index :raids, %i(pincode open)
  end
end
