# frozen_string_literal: true
class AddCanSendToUser < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :can_send, :boolean, default: false
  end
end
