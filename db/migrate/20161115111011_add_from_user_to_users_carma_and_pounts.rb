# frozen_string_literal: true
class AddFromUserToUsersCarmaAndPounts < ActiveRecord::Migration[5.0]
  def change
    add_column :users_carmas, :from_user_id, :integer
    add_column :users_points, :from_user_id, :integer
    add_index :users_carmas, :from_user_id
    add_index :users_points, :from_user_id
  end
end
