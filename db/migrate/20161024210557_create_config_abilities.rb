# frozen_string_literal: true
class CreateConfigAbilities < ActiveRecord::Migration[5.0]
  def change
    create_table :config_abilities do |t|
      t.string :name
      t.string :tech_name
      t.integer :wegth

      t.timestamps
    end
  end
end
