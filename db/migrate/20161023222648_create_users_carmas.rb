# frozen_string_literal: true
class CreateUsersCarmas < ActiveRecord::Migration[5.0]
  def change
    create_table :users_carmas do |t|
      t.belongs_to :user, foreign_key: true
      t.integer :value
      t.text :description

      t.timestamps
    end
  end
end
