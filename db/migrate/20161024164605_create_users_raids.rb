# frozen_string_literal: true
class CreateUsersRaids < ActiveRecord::Migration[5.0]
  def change
    create_table :users_raids do |t|
      t.belongs_to :user, index: true
      t.belongs_to :raid, index: true
      t.belongs_to :role

      t.timestamps
    end

    add_index :users_raids, %i(user_id raid_id)
  end
end
