# frozen_string_literal: true
class CreateAuctions < ActiveRecord::Migration[5.0]
  def change
    create_table :auctions do |t|
      t.belongs_to :item
      t.integer :items_count, default: 1
      t.integer :start_price, default: 0
      t.datetime :close_time
      t.boolean :change_time, default: false

      t.timestamps
    end
  end
end
