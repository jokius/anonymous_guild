# frozen_string_literal: true
class CreateConfigGrades < ActiveRecord::Migration[5.0]
  def change
    create_table :config_grades do |t|
      t.string :name

      t.timestamps
    end
  end
end
