# frozen_string_literal: true
class CreateNotifications < ActiveRecord::Migration[5.0]
  def change
    create_table :notifications do |t|
      t.string :name
      t.string :tech_name

      t.timestamps
    end
  end
end
