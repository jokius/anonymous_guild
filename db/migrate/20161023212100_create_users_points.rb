# frozen_string_literal: true
class CreateUsersPoints < ActiveRecord::Migration[5.0]
  def change
    create_table :users_points do |t|
      t.belongs_to :user, foreign_key: true
      t.integer :value
      t.text :description

      t.timestamps
    end
  end
end
