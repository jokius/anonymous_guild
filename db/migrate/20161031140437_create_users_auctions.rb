# frozen_string_literal: true
class CreateUsersAuctions < ActiveRecord::Migration[5.0]
  def change
    create_table :users_auctions do |t|
      t.belongs_to :user, index: true
      t.belongs_to :auction, index: true
      t.integer :points

      t.timestamps
    end
  end
end
