# frozen_string_literal: true
class CreateUsersNotifications < ActiveRecord::Migration[5.0]
  def change
    create_table :users_notifications do |t|
      t.belongs_to :user, index: true
      t.belongs_to :notification, index: true

      t.timestamps
    end

    add_index :users_notifications, %i(user_id notification_id)
  end
end
