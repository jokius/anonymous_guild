# frozen_string_literal: true
class AddOpenToAuction < ActiveRecord::Migration[5.0]
  def change
    add_column :auctions, :open, :boolean, default: true
  end
end
