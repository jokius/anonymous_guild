# frozen_string_literal: true
class CreateConfigItems < ActiveRecord::Migration[5.0]
  def change
    create_table :config_items do |t|
      t.belongs_to :item_type, index: true
      t.belongs_to :grade, index: true
      t.string :name
      t.string :description

      t.timestamps
    end
  end
end
