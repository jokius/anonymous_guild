# frozen_string_literal: true
[
  { tech_name: :create_config_role, name: 'Создание роли', wegth: 1_000 },
  { tech_name: :update_config_role, name: 'Обновление роли', wegth: 1_000 },
  { tech_name: :delete_config_role, name: 'Удалениие роли', wegth: 1_000 },
  { tech_name: :delete_user, name: 'Удаление пользователя', wegth: 1_000 },
  { tech_name: :view_email_user, name: 'Просмотр email пользователя', wegth: 1_000 },
  { tech_name: :create_config_grade, name: 'Создание грейда', wegth: 1_000 },
  { tech_name: :update_config_grade, name: 'Изменение грейда', wegth: 1_000 },
  { tech_name: :delete_config_grade, name: 'Удаление грейда', wegth: 1_000 },
  { tech_name: :sidekiq, name: 'sidekiq(нужно только Админу)', wegth: 1_000 },
  { tech_name: :show_download_links, name: 'показывать ссылки на выгрузку', wegth: 1_000 },

  { tech_name: :view_config_roles, name: 'Просмотр настроек ролей', wegth: 900 },
  { tech_name: :update_user_nickname, name: 'Изменение ника пользователя', wegth: 900 },
  { tech_name: :create_config_raid, name: 'Создание настроек рейда', wegth: 900 },
  { tech_name: :update_config_raid, name: 'Изменение настроек рейда', wegth: 900 },
  { tech_name: :delete_config_raid, name: 'Удаление настроек рейда', wegth: 900 },
  { tech_name: :view_config_raids, name: 'Просмотр настроек рейдав', wegth: 900 },

  { tech_name: :conf_memu, name: 'Доступ к меню настроек', wegth: 800 },
  { tech_name: :view_config_grades, name: 'Просмотр грейда', wegth: 800 },
  { tech_name: :create_conf_item_type, name: 'Создание типа предмета', wegth: 800 },
  { tech_name: :update_conf_item_type, name: 'Изменение типа предмета', wegth: 800 },
  { tech_name: :delete_conf_item_type, name: 'Удаление типа предмета', wegth: 800 },
  { tech_name: :view_conf_item_type, name: 'Просмотр типов предметов', wegth: 800 },
  { tech_name: :create_conf_item, name: 'Создание предмета', wegth: 800 },
  { tech_name: :update_conf_item, name: 'Изменение предмета', wegth: 800 },
  { tech_name: :delete_conf_item, name: 'Удаление предмета', wegth: 800 },
  { tech_name: :view_conf_item, name: 'Просмотр предметов', wegth: 800 },
  { tech_name: :create_auction, name: 'Создание лота на аукцион', wegth: 800 },

  { tech_name: :activate_user, name: 'Aктивация пользователя', wegth: 700 },
  { tech_name: :block_user, name: 'Блокировка пользователя', wegth: 700 },

  { tech_name: :create_raid, name: 'Создание рейда', wegth: 700 },
  { tech_name: :update_raid, name: 'Изменение рейда', wegth: 700 },
  { tech_name: :delete_raid, name: 'Удаление рейда', wegth: 700 },

  { tech_name: :update_user_roles, name: 'Изменение ролей пользователя', wegth: 600 },
  { tech_name: :add_carma, name: 'Добавление кармы', wegth: 600 },
  { tech_name: :add_points, name: 'Добавление очков', wegth: 600 },
  { tech_name: :remove_carma, name: 'Снятие кармы', wegth: 600 },
  { tech_name: :remove_points, name: 'Снятие очков', wegth: 600 }

].each do |ability|
  Config::Ability.find_or_create_by(tech_name: ability[:tech_name]) do |record|
    record.name = ability[:name]
    record.wegth = ability[:wegth]
  end
end

p 'abilities init complited'

unless Config::Role.any?
  role = Config::Role.create(name: 'Администратор')
  Config::Ability.all.each { |ability| role.roles_abilities.create ability: ability }
  role = Config::Role.create(name: 'Глава гильдии')
  Config::Ability.where('wegth <= 900').each { |ability| role.roles_abilities.create ability: ability }
  role = Config::Role.create(name: 'Казначей')
  Config::Ability.where('wegth <= 800').each { |ability| role.roles_abilities.create ability: ability }
  role = Config::Role.create(name: 'Офицер')
  Config::Ability.where('wegth <= 700').each { |ability| role.roles_abilities.create ability: ability }
  p 'roles created'
end

[
  { tech_name: :raid_mew, name: 'Создан новый рейд' },
  { tech_name: :raid_close, name: 'Рейд завершен' },
  { tech_name: :auction_new, name: 'Добавлен новый лот' },
  { tech_name: :auction_close, name: 'Выйграшная ставка за лот' },
  { tech_name: :carma, name: 'Получение или снятие кармы' },
  { tech_name: :points, name: 'Получение или снятие очков DKP' }
].each do |notification|
  Notification.find_or_create_by(tech_name: notification[:tech_name]) { |record| record.name = notification[:name] }
end

p 'notifications init complited'

unless User.any?
  user = User.create!(email: 'admin@example.com', password: 'password', password_confirmation: 'password',
                      nickname: :remove_me, state: :active)
  user.add_role 'Администратор'
  p 'admin created'
end

unless Config::Raid.any?
  inst = Config::Raid.create name: 'Подземлье 5', carma: 0
  raid = Config::Raid.create name: 'Рейд 10', carma: 10
  inst.roles.create name: 'Лидер', leader: true
  inst.roles.create name: 'Участник', member: true
  raid.roles.create name: 'Лидер рейда', factor_dkp: 1.7, factor_carma: 1.7, leader: true
  raid.roles.create name: 'Лидер группы', factor_dkp: 1.5, factor_carma: 1.5
  raid.roles.create name: 'Участник', member: true
  p 'raid config created'
end

unless Config::ItemType.any?
  %w(Броня Оружие Расходники).each { |name| Config::ItemType.create name: name }
  p 'item type config created'
end

unless Config::Grade.any?
  %w(Серый Белый Зеленый Синий Фиолетовый Золотой Оранжевый).each { |name| Config::Grade.create name: name }
  p 'grades created'
end
