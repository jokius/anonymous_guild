  FROM ruby:2.3.1

MAINTAINER djok69@gmail.com

ARG SSH_KEY

USER root

CMD ["/usr/sbin/sshd", "-D"]

RUN apt-get update && apt-get install -y \
  build-essential \
  locales \
  nodejs

RUN mkdir -p /web
WORKDIR /web

COPY Gemfile Gemfile.lock ./
ENV BUNDLE_GEMFILE=./Gemfile \
  BUNDLE_JOBS=2 \
  BUNDLE_PATH=/bundle

COPY . ./
