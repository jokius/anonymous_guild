# frozen_string_literal: true
class RaidMailerPreview < ActionMailer::Preview
  def close
    user = User.first
    points = user.points.find_by(name: :raid)
    carma = user.carma.find_by(name: :raid)
    RaidMailer.close(user, points, carma)
  end

  def open
    user = User.first
    raid = Raid.last
    RaidMailer.open(user, raid)
  end
end
