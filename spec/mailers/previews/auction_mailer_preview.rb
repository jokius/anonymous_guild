# frozen_string_literal: true
class AuctionMailerPreview < ActionMailer::Preview
  def open
    user = User.first
    auction = Auction.last
    AuctionMailer.open(user, auction)
  end

  def close
    user = User.first
    auction = Auction.first
    AuctionMailer.close(user, auction)
  end
end
