# frozen_string_literal: true
class UserMailerPreview < ActionMailer::Preview
  def change_carma_or_points
    user = User.first
    UserMailer.change_carma_or_points(user, 'ololo')
  end
end
