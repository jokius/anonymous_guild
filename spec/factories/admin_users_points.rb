FactoryGirl.define do
  factory :admin_users_point, class: 'AdminUsers::Point' do
    admin_user nil
    value 1
  end
end
